#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os

from subprocess import run

root_folder = os.path.realpath(os.path.abspath(os.path.join(
    os.path.normpath(os.path.dirname(__file__)))))


class ANSIColors():

    def ERROR(self, string):
        return "\033[38;5;166;1m" + str(string) + "\033[0m"

    def INFO(self, string):
        return "\033[1m" + str(string) + "\033[0m"

    def WARNING(self, string):
        return "\033[38;5;220;1m" + str(string) + "\033[0m"

    def SUCCESS(self, string):
        return "\033[38;5;77;1m" + str(string) + "\033[0m"

    def PURPLE(self, string):
        return "\033[38;5;164;1m" + str(string) + "\033[0m"


Ansi = ANSIColors()

_dist_folder = os.path.join(root_folder, "dist")
_variants = [
    "flatly_mod",
    "darkly_mod"
]


def _get_environment(set_vars={}, unset_vars=[]):
    env = {}
    env.update(os.environ)

    if set_vars:
        env.update(set_vars)

    if unset_vars:
        for var in unset_vars:
            del env[var]

    return env


def _can_exec(path):
    return os.path.isfile(path) and os.access(path, os.X_OK)


def _find_executables(executable):
    env = _get_environment()

    for base in env.get("PATH", "").split(os.pathsep):
        path = os.path.join(os.path.expanduser(base), executable)

        if _can_exec(path):
            yield path

    return None


def _which(cmd):
    for path in _find_executables(cmd):
        return path

    return None


if __name__ == "__main__":
    if _which("sass"):
        sass_cmd = ["sass"]
        sass_cmd_arg_files = "%s:%s"
        sass_cmd_arg_2 = ["--no-source-map"]
    elif _which("sassc"):
        sass_cmd = ["sassc"]
        sass_cmd_arg_files = "%s %s"
        sass_cmd_arg_2 = []
    else:
        print(Ansi.ERROR("Missing sass command. Read the README."))
        raise SystemExit(1)

    # Change the current working directory because the last comamnd needs to be
    # executed from the repository's directory.
    os.chdir(root_folder)

    for variant in _variants:
        sass_file = os.path.join(root_folder, "%s.scss" % variant)
        css_file_expanded = os.path.join(_dist_folder, "%s.css" % variant)
        css_file_compressed = os.path.join(_dist_folder, "%s.min.css" % variant)

        os.makedirs(_dist_folder, exist_ok=True)

        print(Ansi.INFO("Building expanded stylesheet for variant: %s" % variant))
        final_cmd = " ".join(sass_cmd + [sass_cmd_arg_files % (sass_file, css_file_expanded)
                                         ] + ["--style=expanded"] + sass_cmd_arg_2)
        print(Ansi.INFO("Executed command:"))
        print(final_cmd)
        run(final_cmd, shell=True)
        print()

        print(Ansi.INFO("Building compressed stylesheet for variant: %s" % variant))
        final_cmd = " ".join(sass_cmd + [sass_cmd_arg_files % (sass_file, css_file_compressed)
                                         ] + ["--style=compressed"] + sass_cmd_arg_2)
        print(Ansi.INFO("Executed command:"))
        print(final_cmd)
        run(final_cmd, shell=True)
        print()

        try:
            run(final_cmd, shell=True)
        except Exception:
            raise SystemExit(
                "Either Node.js or the required node modules are missing. Read the README.")

    print(Ansi.INFO("Autoprefixing generated CSS files..."))
    cmd = "npx postcss dist/*.css --dir dist/"
    print(Ansi.INFO("Executed command:"))
    print(cmd)

    try:
        run(cmd, shell=True)
    except Exception:
        raise SystemExit(
            "Either Node.js or the required node modules are missing. Read the README.")
