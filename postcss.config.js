module.exports = {
    plugins: [
        require("autoprefixer")({
            add: true,
            remove: false,
            supports: false,
            cascade: true,
            flexbox: true,
            grid: true,
            overrideBrowserslist: [
                "Chrome >= 35",
                "Firefox >= 38",
                "Edge >= 12",
                "Explorer >= 9",
                "iOS >= 8",
                "Safari >= 8",
                "Android 2.3",
                "Android >= 4",
                "Opera >= 12"
            ]
        })
    ]
};
